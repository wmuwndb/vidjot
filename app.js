const express = require('express')
const hbs = require('express-handlebars')
const methodOverride = require('method-override')
const flash = require('connect-flash')
const session = require('express-session')
const bodyParser = require('body-parser')
const mongoose = require('mongoose')

const app = express()

// Connect to mongo
mongoose.connect('mongodb://localhost/vidjot-dev', {
  useNewUrlParser: true
})
  .then(() => console.log('MongoDB connected...'))
  .catch(err => console.log(err))

// Load idea model
require('./models/Idea')
const Idea = mongoose.model('ideas')

// Handlebar middleware
app.engine('handlebars', hbs({
  defaultLayout: 'main'
}))
app.set('view engine', 'handlebars')

// Body Parser

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))
// parse application/json
app.use(bodyParser.json())

// Method override middleware
app.use(methodOverride('_method'))

// Session middleware
app.use(session({
  secret: 'keyboard cat',
  resave: true,
  saveUninitialized: true
}))

// Flash middleware
app.use(flash())

app.use((req, res, next) => {
  res.locals.success_msg = req.flash('success_msg')
  res.locals.error_msg = req.flash('error_msg')
  res.locals.error = req.flash('error')
  next()
})

// Index route
app.get('/', (req, res) => {
  const title = 'Welcome to Vidjot'
  res.render('index', {
    title
  })
})

// About route
app.get('/about', (req, res) => {
  res.render('about')
})

// Idea index page
app.get('/ideas', (req, res) => {
  Idea.find({})
    .sort({ date: 'desc' })
    .then(ideas => {
      res.render('ideas/index', { ideas })
    })
})

// Add idea form
app.get('/ideas/add', (req, res) => {
  res.render('ideas/add')
})

// Edit idea form
app.get('/ideas/edit/:id', (req, res) => {
  Idea.findOne({
    _id: req.params.id
  })
    .then(idea => {
      res.render('ideas/edit', { idea })
    })
})

// Process form
app.post('/ideas', (req, res) => {
  const errors = []

  if (!req.body.title) {
    errors.push({ text: 'Please add a title' })
  }

  if (!req.body.details) {
    errors.push({ text: 'Please add some details' })
  }

  if (errors.length > 0) {
    res.render('ideas/add', {
      errors,
      title: req.body.title,
      details: req.body.details
    })
  } else {
    const newUser = {
      title: req.body.title,
      details: req.body.details
    }

    new Idea(newUser)
      .save()
      .then(idea => {
        req.flash('success_msg', 'Video idea added')
        res.redirect('/ideas')
      })
  }
})

// Edit form process
app.put('/ideas/:id', (req, res) => {
  Idea.findOne({
    _id: req.params.id
  })
    .then(idea => {
      // new values
      idea.title = req.body.title
      idea.details = req.body.details

      idea.save()
    })
    .then(idea => {
      // idea saved
      req.flash('success_msg', 'Video idea updated')
      res.redirect('/ideas')
    })
})

// Delete idea
app.delete('/ideas/:id', (req, res) => {
  Idea.deleteOne({
    _id: req.params.id
  })
    .then(() => {
      req.flash('success_msg', 'Video idea removed')
      res.redirect('/ideas')
    })
})

// use the PORT env variable if available
const PORT = process.env.PORT || 5000

app.listen(PORT, () => {
  console.log(`Server started on ${PORT}`)
})
